#ifndef __eccplus_included__
#error "Must include eccplus.c first"
#endif
coor eccsub(ec ell, coor ina, coor inb) {
    coor minb,answer;
    minb.x = inb.x;
    minb.y = -inb.y;
    minb.maxi = inb.maxi;
    answer = eccplus(ell,ina,minb);
    return answer;
}
