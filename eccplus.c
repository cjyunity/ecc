/*This function is for plus operation on an elliptic curve.
 *y^2 + axy + by = x^3 + cx^2 + dx +e
 */
#define __eccplus_included__
coor eccplus(ec ell, coor ina, coor inb) {
    if (ina.maxi == true) {
        return inb;
    }
    if (inb.maxi == true) {
        return ina;
    }	
    double x1,y1,x2,y2,a,b;
    x1 = ina.x;
    y1 = ina.y;
    x2 = inb.x;
    y2 = inb.y;
    a = ell.a;
    b = ell.b;
    double k;
    coor answer;
    answer.maxi = false;
    if ((x1 == x2)&&(y1 == y2)) {
        k = (3*x1*x1 + a) / 2*y1;
    }
    if ((x1 == x2)&&(y1 != y2)) {
        answer.maxi = true;
    }
    if ((x1 != x2)&&(y1 != y2)) {
        k = (y1 - y2) / (x1 - x2);
    }
    double x,y;
    x = k*k - x1 - x2;
    y = y1 + k*x - k*x1;
    answer.x = x;
    answer.y = y;
    return answer;
}

